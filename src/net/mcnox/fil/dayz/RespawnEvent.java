package net.mcnox.fil.dayz;

import net.minecraft.server.v1_6_R3.EntityCreature;

import org.bukkit.craftbukkit.v1_6_R3.entity.CraftCreature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class RespawnEvent implements Listener
{
	public MineZ plugin;
	
	public RespawnEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e)
	{
		Player p = e.getPlayer();
		p.teleport(p.getWorld().getSpawnLocation());
	}
	
	@EventHandler
	public void onEntitySpawn(CreatureSpawnEvent e)
	{
		Entity ent = e.getEntity();
		if(ent.getType() != EntityType.ZOMBIE)
		{
			e.setCancelled(true);
		}
		else
		{
			EntityCreature ec = ((CraftCreature) ent).getHandle();
			ec.setSprinting(true);
		}
	}
}
