package net.mcnox.fil.dayz;

import net.minecraft.server.v1_6_R3.EntityCreature;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_6_R3.entity.CraftCreature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class DeathEvent implements Listener
{
	private MineZ plugin;
	
	public DeathEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e)
	{
		if(e.getEntity() instanceof Player)
		{
			Player p = (Player) e.getEntity();
			e.getDrops().clear();
			e.setDroppedExp(0);
			if(plugin.town.containsKey(p.getName()))
			{
				plugin.town.remove(p.getName());
			}
			plugin.town.remove(p.getName());
			EntityDamageEvent damageEvent = p.getLastDamageCause();
			if(!(damageEvent instanceof EntityDamageByEntityEvent))
			    return;
			Entity damager = ((EntityDamageByEntityEvent)damageEvent).getDamager();
			if(damager instanceof Zombie)
			{
				e.setDeathMessage("§e" + p.getName() + " §cWas Bitten by a Zombie!");
				plugin.killedZombies.put(p.getName(), 0);
				plugin.killedZombies.remove(p.getName());
				Zombie zombie = (Zombie) p.getWorld().spawnEntity(p.getLocation(), EntityType.ZOMBIE);
				ItemStack head = new ItemStack(397, 1, (short) 3);
				SkullMeta meta = (SkullMeta) head.getItemMeta();
				meta.setOwner(p.getName());
				head.setItemMeta(meta);
				EntityEquipment equipment = zombie.getEquipment();
				equipment.setHelmet(head);
				equipment.setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE, 1));
				equipment.setLeggings(new ItemStack(Material.LEATHER_LEGGINGS, 1));
				equipment.setBoots(new ItemStack(Material.LEATHER_BOOTS, 1));
				equipment.setItemInHand(new ItemStack(Material.STONE_SWORD, 1));
				zombie.setCustomName(p.getName());
				zombie.setCustomNameVisible(true);
				EntityCreature ec = ((CraftCreature) zombie).getHandle();
				ec.setSprinting(true);
			}
			else if(damager instanceof Player)
			{
				Player d = (Player) damager;
				e.setDeathMessage("§e" + d.getName() + " §cMURDERED " + "§e" + p.getName() + " §cwith a " + "§e" + p.getKiller().getItemInHand().getType().toString().replace("_", " ").toLowerCase() + "!");
			}
		}
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent e)
	{
		Entity ent = e.getEntity();
		if(ent.getType() == EntityType.ZOMBIE)
		{
			if(e.getEntity().getKiller() != null && e.getEntity().getKiller() instanceof Player)
			{
				Player p = e.getEntity().getKiller();
				if(!plugin.killedZombies.containsKey(p.getName()))
				{
					plugin.killedZombies.put(p.getName(), 1);
					e.setDroppedExp(0);
					p.sendMessage(ChatColor.GOLD + "Zombie Down! Total this Life: " + plugin.killedZombies.get(p.getName()));
				}
				else
				{
					int zombies = plugin.killedZombies.get(p.getName()) + 1;
					plugin.killedZombies.put(p.getName(), zombies);
					e.setDroppedExp(0);
					p.sendMessage(ChatColor.GOLD + "Zombie Down! Total this Life: " + plugin.killedZombies.get(p.getName()));
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityFire(EntityCombustEvent e)
	{
		Entity ent = e.getEntity();
		if(ent.getType() == EntityType.ZOMBIE)
		{
			ent.setFireTicks(0);
			e.setCancelled(true);
		}
	}
}
