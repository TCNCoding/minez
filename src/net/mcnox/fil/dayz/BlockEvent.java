package net.mcnox.fil.dayz;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockEvent implements Listener
{
	public MineZ plugin;
	
	public BlockEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e)
	{
		Player p = e.getPlayer();
		if(!p.hasPermission("minez.admin"))
		{
			p.sendMessage(ChatColor.RED + "Glitching with blocks can get you banned.");
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e)
	{
		Player p = e.getPlayer();
		if(!p.hasPermission("minez.admin"))
		{
			p.sendMessage(ChatColor.RED + "Glitching with blocks can get you banned.");
			e.setCancelled(true);
		}
	}
}
