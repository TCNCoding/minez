package net.mcnox.fil.dayz;

import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_6_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class InteractEvent implements Listener
{
	private MineZ plugin;
	
	public InteractEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e)
	{
		Player p = e.getPlayer();
		
		if(e.getClickedBlock().getType() == Material.CHEST)
		{
			if(e.getAction() == Action.LEFT_CLICK_BLOCK)
			{
				return;
			}
			
			plugin.startChestTimer();
		}
		if(e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN || e.getClickedBlock().getType() == Material.SIGN)
		{
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK)
			{
				Sign sign = (Sign) e.getClickedBlock().getState();
				if(sign.getLine(1).equalsIgnoreCase("To Premium area"))
				{
					PermissionUser user = PermissionsEx.getUser(p);
					String group = sign.getLine(2).replace("(", "").replace(")", "");
					if(user.inGroup(group))
					{
						p.teleport(new Location(p.getWorld(), 604, 39, 573));
					}
					else
					{
						p.sendMessage(ChatColor.RED + "Seems like you are not in that Required Group to Run this Command.");
						p.sendMessage("I bet your wondering whats in the Premium Area.");
					}
				}
			}
		}
		if(p.getItemInHand().getType() == Material.EYE_OF_ENDER)
		{
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
			{
				Date time = new Date();
				time.setSeconds(0);
				if(time.getSeconds() == 0)
				{
					p.sendMessage(ChatColor.RED + "Logout after 20 Seconds");
				}
				if(time.getSeconds() == 5)
				{
					p.sendMessage(ChatColor.RED + "Logout after 15 Seconds");
				}
				if(time.getSeconds() == 10)
				{
					p.sendMessage(ChatColor.RED + "Logout after 10 Seconds");
				}
				if(time.getSeconds() == 11)
				{
					p.sendMessage(ChatColor.RED + "Logout after 9 Seconds");
				}
				if(time.getSeconds() == 12)
				{
					p.sendMessage(ChatColor.RED + "Logout after 8 Seconds");
				}
				if(time.getSeconds() == 13)
				{
					p.sendMessage(ChatColor.RED + "Logout after 7 Seconds");
				}
				if(time.getSeconds() == 14)
				{
					p.sendMessage(ChatColor.RED + "Logout after 6 Seconds");
				}
				if(time.getSeconds() == 15)
				{
					p.sendMessage(ChatColor.RED + "Logout after 5 Seconds");
				}
				if(time.getSeconds() == 16)
				{
					p.sendMessage(ChatColor.RED + "Logout after 4 Seconds");
				}
				if(time.getSeconds() == 17)
				{
					p.sendMessage(ChatColor.RED + "Logout after 3 Seconds");
				}
				if(time.getSeconds() == 18)
				{
					p.sendMessage(ChatColor.RED + "Logout after 2 Seconds");
				}
				if(time.getSeconds() == 19)
				{
					p.sendMessage(ChatColor.RED + "Logout after 1 Seconds");
				}
				if(time.getSeconds() == 20)
				{
					p.kickPlayer("Disconnected from MineZ!");
				}
			}
		}
		else if(p.getItemInHand().getType() == Material.PAPER)
		{
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
			{
				if(((CraftPlayer) p).getHandle().getHealth() == 20)
				{
					p.getInventory().remove(Material.PAPER);
					p.sendMessage(ChatColor.DARK_RED + "Well, that was kind of a waste");
				}
				else if(((CraftPlayer) p).getHandle().getHealth() > 14)
				{
					p.getInventory().remove(Material.PAPER);
					double health = ((CraftPlayer) p).getHandle().getHealth() + 2;
					p.setHealth(health);
					p.sendMessage(ChatColor.DARK_RED + "Not a bad fix.");
				}
				else
				{
					p.getInventory().remove(Material.PAPER);
					double health = ((CraftPlayer) p).getHandle().getHealth() + 2;
					p.setHealth(health);
					p.sendMessage(ChatColor.DARK_RED + "This should patch me up until I get something more permanent");
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerItemConsume(PlayerItemConsumeEvent e)
	{
		Player p = e.getPlayer();
		if(e.getItem().getTypeId() == 373)
		{
			p.sendMessage(ChatColor.AQUA + "Ah, much better!");
			p.setLevel(20);
		}
	}
}
