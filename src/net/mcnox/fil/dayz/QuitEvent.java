package net.mcnox.fil.dayz;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitEvent implements Listener
{
	private MineZ plugin;
	
	public QuitEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		Player p = e.getPlayer();
		if(plugin.town.containsKey(p.getName()))
		{
			plugin.town.remove(p.getName());
		}
		plugin.killedZombies.put(p.getName(), 0);
		plugin.killedZombies.remove(p.getName());
		plugin.localDeathMessages.remove(p.getName());
		plugin.globalDeathMessages.remove(p.getName());
	}
}
