package net.mcnox.fil.dayz;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveEvent implements Listener
{
	private MineZ plugin;
	public int sprintID;
	
	public MoveEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e)
	{
		final Player p = e.getPlayer();
		if(e.getTo().getBlock().equals(Material.PISTON_EXTENSION))
		{
			if(plugin.town.containsKey(p.getName()))
			{
				e.getPlayer().teleport(e.getFrom());
				p.sendMessage(ChatColor.RED + "You have reached the Border of this Map!");
			}
		}
		if(p.isSprinting())
		{
			p.setExp(0.896F);
		}
		else
		{
			p.setExp(0.672F);
		}
		sprintID = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
		public void run()
		{
			if(p.isSprinting())
			{
				int level = p.getLevel();
				level -= 1;
				p.setLevel(level);
			}
		}
	}, 0L, 100L);
	}
}
