package net.mcnox.fil.dayz;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

//import ru.tehkode.permissions.PermissionUser;
//import ru.tehkode.permissions.bukkit.PermissionsEx;

public class JoinEvent implements Listener
{
	private MineZ plugin;
	
	public JoinEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();
		p.setPlayerTime(0, false);
		plugin.localDeathMessages.add(p.getName());
		//PermissionUser user = PermissionsEx.getUser(p);
		plugin.startChestTimer();
		p.setLevel(20);
	}
}
