package net.mcnox.fil.dayz;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatEvent implements Listener
{
	private MineZ plugin;
	
	public ChatEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e)
	{
		Player p = e.getPlayer();
		if(p.getItemInHand().getType() == Material.EYE_OF_ENDER)
		{
			e.setFormat(p.getName() + ":" + "§a Eye[" + plugin.getNearbyTown(p) + "] " + e.getMessage());
		}
		else
		{
			e.setFormat(p.getName() + ": " + e.getMessage());
		}
	}
}
