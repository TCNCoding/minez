package net.mcnox.fil.dayz;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_6_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class DamageEvent implements Listener
{
	private MineZ plugin;
	
	public DamageEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerAttack(EntityDamageEvent e)
	{
		Entity ent = e.getEntity();
		if(ent.getType() == EntityType.ZOMBIE)
		{
			World world = ent.getWorld();
		    Location entityLoc = ent.getLocation();
		    Location loc = new Location(entityLoc.getWorld(), entityLoc.getX(), entityLoc.getY() + 0.4D, entityLoc.getZ());
		    ent.getWorld().playSound(loc, Sound.HURT_FLESH, 1.0F, 1.0F);
		    world.playEffect(loc, Effect.STEP_SOUND, Material.ENDER_STONE);
		}
		else if(ent.getType() == EntityType.PLAYER)
		{
			Player p = (Player) ent;
			if(!plugin.town.containsKey(p.getName()))
			{
				e.setCancelled(true);
			}
			else
			{
				e.setCancelled(false);
				World world = ent.getWorld();
			    Location entityLoc = ent.getLocation();
			    Location loc = new Location(entityLoc.getWorld(), entityLoc.getX(), entityLoc.getY() + 0.4D, entityLoc.getZ());
			    ent.getWorld().playSound(loc, Sound.HURT_FLESH, 1.0F, 1.0F);
			    world.playEffect(loc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			    if(((CraftPlayer) p).getHandle().getHealth() < 2)
			    {
			    	p.sendMessage(ChatColor.DARK_RED + "That hit me harder than expected... I think I'm bleeding");
			    	PotionEffect blindness = new PotionEffect(PotionEffectType.BLINDNESS, 1, 1);
			    	p.addPotionEffect(blindness);
			    	p.sendMessage(ChatColor.DARK_RED + "That hurts...I need to find a bandage to stop the bleeding!");
			    }
			}
		}
	}
	
	@EventHandler
    public void onPlayerRegainHealth(EntityRegainHealthEvent e) {
        if(e.getRegainReason() == RegainReason.SATIATED || e.getRegainReason() == RegainReason.REGEN)
        if(e.getEntity() instanceof Player)
        {
        	Player p = (Player) e.getEntity();
        	if(((CraftPlayer) p).getHandle().getHealth() < 1)
        	{
        		p.sendMessage(ChatColor.DARK_RED + "That hurts...I need to find a bandage to stop the bleeding!");
        		PotionEffect blindness = new PotionEffect(PotionEffectType.BLINDNESS, 1, 1);
        		p.addPotionEffect(blindness);
        	}
            e.setCancelled(true);
        }
    }
}
