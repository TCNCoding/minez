package net.mcnox.fil.dayz;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class MineZ extends JavaPlugin implements Listener
{
	public File configFile;
	public FileConfiguration config;
	public HashMap<String, Integer> killedZombies = new HashMap<String, Integer>();
	public HashMap<String, Integer> town = new HashMap<String, Integer>();
	public ArrayList<String> localDeathMessages = new ArrayList<String>();
	public ArrayList<String> globalDeathMessages = new ArrayList<String>();
	private int updaterID;
	public int chestID;
	Updater updater = new Updater(this);
	
	public void onEnable()
	{
		updater.checkForUpdates();
		configFile = new File(getDataFolder() + File.separator + "config.yml");
		if(!getDataFolder().exists())
		{
			getDataFolder().mkdir();
		}
		if(!configFile.exists())
		{
			try {
				configFile.createNewFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		config = YamlConfiguration.loadConfiguration(configFile);
		Bukkit.getPluginManager().registerEvents(new ChatEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new DamageEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new DeathEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new DropEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new InteractEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new JoinEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new MoveEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new QuitEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new RespawnEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockEvent(this), this);
		Bukkit.getPluginManager().registerEvents(this, this);
	}
	
	public void onDisable()
	{
		try {
			config.save(configFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void loadLootConfig() {

		// SET HEADER

		this.getLootConfig().options().header("++=======================================++"
								 		  + "\n|| Loot for the Wasted plugin my terturl ||"
								 		  + "\n++=======================================++");



		// LOOT

			// SETTINGS
			getLootConfig().addDefault("Loot.settings.time-before-refill", 120);
			getLootConfig().addDefault("Loot.settings.min-stacks-filled", 1);
			getLootConfig().addDefault("Loot.settings.max-stacks-filled", 3);

			// LISTS

			String[] value_lists_all = {
				"30", "46", "2x39", "373:16389"
			};
			getLootConfig().addDefault("Loot.lists.all", value_lists_all);


			//To add more loot stuff
			
			/*&String[] value_lists_military = {
				"30", "258"
			};
			getLootConfig().addDefault("Loot.lists.military", value_lists_military);*/
			
		getLootConfig().options().copyDefaults(true);
		saveLootConfig();

	}





	private FileConfiguration lootConfig = null;
	private File lootConfigFile = null;

	public void reloadLootConfig() {
		if (lootConfigFile == null) lootConfigFile = new File(this.getDataFolder(), "loot.yml");
		lootConfig = YamlConfiguration.loadConfiguration(lootConfigFile);
		loadLootConfig();

	}

	public FileConfiguration getLootConfig() {
		if (lootConfig == null) reloadLootConfig();
		return lootConfig;
	}

	public void saveLootConfig() {

		if (lootConfig == null || lootConfigFile == null) return;

		try {
			getLootConfig().save(lootConfigFile);
		} catch (IOException ex) {
			getLogger().log(Level.SEVERE, "Could not save config to " + lootConfigFile, ex);
		}

	}
	 
	 public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
		{
			if(sender instanceof Player)
			{
				Player p = (Player) sender;
				if(cmd.getName().equalsIgnoreCase("kill"))
				{
					
				}
				if(cmd.getName().equalsIgnoreCase("setworldspawn"))
				{
					if(p.hasPermission("minez.setspawn"))
					{
						World w = p.getWorld();
						w.setSpawnLocation(p.getLocation().getBlockX(), p.getLocation().getBlockY(), p.getLocation().getBlockZ());
					}
				}
				else if(cmd.getName().equalsIgnoreCase("minez"))
				{
					if(args.length == 0)
					{
						p.sendMessage("/minez spawn: teleport to one of the spawn points.");
						p.sendMessage("/minez server: find out which server this is.");
						p.sendMessage("/minez top10: show the top10 most successful players.");
						p.sendMessage("/minez deathmessages: toggle between global and local death");
						p.sendMessage("messages");
					}
					else if(args[0].equalsIgnoreCase("spawn"))
					{
						if(args.length == 2)
						{
							int townNum = Integer.parseInt(args[1]);
							List<String> towns = config.getStringList("towns");
							if(towns.contains(townNum))
							{
								if(!town.containsKey(p.getName()))
								{
									p.sendMessage(ChatColor.RED + "You are now in the world, good luck.");
									town.put(p.getName(), townNum);
									ItemStack waterBottle = new ItemStack(Material.getMaterial(373), 1);
									ItemStack woodSword = new ItemStack(Material.WOOD_SWORD, 1);
									ItemStack bandage = new ItemStack(Material.PAPER, 1);
									ItemStack helmet = new ItemStack(Material.LEATHER_HELMET, 1);
									ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
									ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
									ItemStack boots = new ItemStack(Material.LEATHER_BOOTS, 1);
									ItemMeta waterMeta = waterBottle.getItemMeta();
									ItemMeta swordMeta = woodSword.getItemMeta();
									ItemMeta bandageMeta = bandage.getItemMeta();
									ItemMeta helmetMeta = helmet.getItemMeta();
									ItemMeta chestplateMeta = chestplate.getItemMeta();
									ItemMeta leggingsMeta = leggings.getItemMeta();
									ItemMeta bootsMeta = boots.getItemMeta();
									waterMeta.setLore(Arrays.asList("§6Soulbound"));
									swordMeta.setLore(Arrays.asList("§6Soulbound"));
									bandageMeta.setDisplayName("§fBandage");
									bandageMeta.setLore(Arrays.asList("§6Soulbound"));
									helmetMeta.setLore(Arrays.asList("§6Soulbound"));
									chestplateMeta.setLore(Arrays.asList("§6Soulbound"));
									leggingsMeta.setLore(Arrays.asList("§6Soulbound"));
									bootsMeta.setLore(Arrays.asList("§6Soulbound"));
									waterBottle.setItemMeta(waterMeta);
									woodSword.setItemMeta(swordMeta);
									bandage.setItemMeta(bandageMeta);
									boots.setItemMeta(bootsMeta);
									leggings.setItemMeta(leggingsMeta);
									chestplate.setItemMeta(chestplateMeta);
									helmet.setItemMeta(helmetMeta);
									p.getInventory().addItem(waterBottle);
									p.getInventory().addItem(woodSword);
									p.getInventory().addItem(bandage);
									p.getInventory().addItem(new ItemStack(Material.EYE_OF_ENDER, 1));
									p.getInventory().setHelmet(helmet);
									p.getInventory().setChestplate(chestplate);
									p.getInventory().setLeggings(leggings);
									p.getInventory().setBoots(boots);
									PotionEffect blindness = new PotionEffect(PotionEffectType.BLINDNESS, 2, 1);
									PotionEffect nausea = new PotionEffect(PotionEffectType.CONFUSION, 4, 1);
									p.addPotionEffect(blindness);
									p.addPotionEffect(nausea);
									p.teleport(new Location(Bukkit.getWorld("world"), 845, 62, 456));
								}
								else
								{
									p.sendMessage("You are too far away from spawn.");
								}
							}
						}
						else
						{
							if(!town.containsKey(p.getName()))
							{
								p.sendMessage(ChatColor.RED + "You are now in the world, good luck.");
								town.put(p.getName(), 1);
								ItemStack waterBottle = new ItemStack(Material.getMaterial(373), 1);
								ItemStack woodSword = new ItemStack(Material.WOOD_SWORD, 1);
								ItemStack bandage = new ItemStack(Material.PAPER, 1);
								ItemStack helmet = new ItemStack(Material.LEATHER_HELMET, 1);
								ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
								ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
								ItemStack boots = new ItemStack(Material.LEATHER_BOOTS, 1);
								ItemMeta waterMeta = waterBottle.getItemMeta();
								ItemMeta swordMeta = woodSword.getItemMeta();
								ItemMeta bandageMeta = bandage.getItemMeta();
								ItemMeta helmetMeta = helmet.getItemMeta();
								ItemMeta chestplateMeta = chestplate.getItemMeta();
								ItemMeta leggingsMeta = leggings.getItemMeta();
								ItemMeta bootsMeta = boots.getItemMeta();
								waterMeta.setLore(Arrays.asList("§6Soulbound"));
								swordMeta.setLore(Arrays.asList("§6Soulbound"));
								bandageMeta.setDisplayName("Bandage");
								bandageMeta.setLore(Arrays.asList("§6Soulbound"));
								helmetMeta.setLore(Arrays.asList("§6Soulbound"));
								chestplateMeta.setLore(Arrays.asList("§6Soulbound"));
								leggingsMeta.setLore(Arrays.asList("§6Soulbound"));
								bootsMeta.setLore(Arrays.asList("§6Soulbound"));
								waterBottle.setItemMeta(waterMeta);
								woodSword.setItemMeta(swordMeta);
								bandage.setItemMeta(bandageMeta);
								boots.setItemMeta(bootsMeta);
								leggings.setItemMeta(leggingsMeta);
								chestplate.setItemMeta(chestplateMeta);
								helmet.setItemMeta(helmetMeta);
								p.getInventory().addItem(waterBottle);
								p.getInventory().addItem(woodSword);
								p.getInventory().addItem(bandage);
								p.getInventory().addItem(new ItemStack(Material.EYE_OF_ENDER, 1));
								p.getInventory().setHelmet(helmet);
								p.getInventory().setChestplate(chestplate);
								p.getInventory().setLeggings(leggings);
								p.getInventory().setBoots(boots);
								PotionEffect blindness = new PotionEffect(PotionEffectType.BLINDNESS, 2, 1);
								PotionEffect nausea = new PotionEffect(PotionEffectType.CONFUSION, 4, 1);
								p.addPotionEffect(blindness);
								p.addPotionEffect(nausea);
								p.teleport(new Location(Bukkit.getWorld("world"), 845, 62, 456));
							}
							else
							{
								p.sendMessage("You are too far away from spawn.");
							}
						}
						}
					}
					else if(args[0].equalsIgnoreCase("server"))
					{
						p.sendMessage(ChatColor.GREEN + "You are on " + Bukkit.getServerName());
					}
					else if(args[0].equalsIgnoreCase("top10"))
					{
						p.sendMessage(ChatColor.RED + "The Top10 Feature is Coming Soon!");
					}
					else if(args[0].equalsIgnoreCase("deathmessages"))
					{
						if(localDeathMessages.contains(p.getName()))
						{
							globalDeathMessages.add(p.getName());
							localDeathMessages.remove(p.getName());
							p.sendMessage("Now receiving global death messages");
						}
						else
						{
							localDeathMessages.add(p.getName());
							globalDeathMessages.remove(p.getName());
							p.sendMessage("No longer receiving global death messages");
						}
					}
				}
			return false;
		}
	 
	 public int getNearbyTown(Player p)
	 {
		 return town.get(p.getName());
	 }
	 
	 public void startUpdateTimer()
	 {
		 updaterID = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			 public void run()
			 {
				 updater.checkForUpdatesIngame();
			 }
		 }, 0L, 200L);
	 }
	 
	 @EventHandler
	 public void onPluginEnable(PluginEnableEvent e)
	 {
		 startUpdateTimer();
	 }
	 
	 @EventHandler
	 public void onPluginDisable(PluginDisableEvent e)
	 {
		 Bukkit.getScheduler().cancelTask(updaterID);
	 }
	 
	 public void startChestTimer()
	 {
		 chestID = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			 public void run()
			 {
				 for(Chunk chunk : Bukkit.getWorld("world").getLoadedChunks())
				 {
					 for(BlockState bs : chunk.getTileEntities())
					 {
						 if(bs instanceof Chest)
						 {
							 Chest chest = (Chest) bs;
							 for(ItemStack stack : chest.getInventory().getContents())
							 {
								if(stack == null)
								{
									int itemIDMin = 1;
									int itemIDMax = 403;
									int itemID = (int)(Math.random() * (itemIDMin-itemIDMax))+itemIDMin;
									int amountMin = 1;
									int amountMax = 12;
									int amount = (int)(Math.random() * (amountMin-amountMax))+amountMin;
									short damageMin = 1;
									short damageMax = 15;
									short damage = (short) ((short)(Math.random() * (damageMin-damageMax))+damageMin);
									chest.getInventory().addItem(new ItemStack(itemID, amount, damage));
									chest.getInventory().addItem(new ItemStack(itemID, amount, damage));
									chest.getInventory().addItem(new ItemStack(itemID, amount, damage));
									chest.getInventory().addItem(new ItemStack(itemID, amount, damage));
								}
							 }
						 }
					 }
				 }
			 }
		 }, 0L, 2400L);
	 }
}
