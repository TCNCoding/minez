package net.mcnox.fil.dayz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Updater {
  	private final String dlLink = "http://files.enjin.com/485118/plugins/MineZ.jar";
	private final String versionLink = "http://files.enjin.com/485118/plugins/minez-version.txt";
	private Plugin plugin;
	private int restartID;
	private int restartTime = 30;
	
	public Updater(Plugin plugin) {
		this.plugin = plugin;
	}
	
	public void checkForUpdates() {
		int oldVersion = this.getVersionFromString(plugin.getDescription().getVersion());
		String path = this.getFilePath();
		
		try {
			URL url = new URL(versionLink);
			URLConnection con = url.openConnection();
			InputStreamReader isr = new InputStreamReader(con.getInputStream());
			BufferedReader reader = new BufferedReader(isr);
			int newVersion = this.getVersionFromString(reader.readLine());
			reader.close();
			
			if(newVersion > oldVersion) {
				url = new URL(dlLink);
				con = url.openConnection();
				InputStream in = con.getInputStream();
				FileOutputStream out = new FileOutputStream(path);
				byte[] buffer = new byte[1024];
				int size = 0;
				while((size = in.read(buffer)) != -1) {
					out.write(buffer, 0, size);
				}
				
				out.close();
				in.close();
				Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "Succesfully updated plugin to v" + newVersion);
				Bukkit.shutdown();
			}
			else
			{
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "No Updates Found!");
			}
		} catch(IOException e) {
			plugin.getLogger().log(Level.SEVERE, "Failed to auto-update", e);
		}
	}
	
	public void checkForUpdatesIngame() {
		int oldVersion = this.getVersionFromString(plugin.getDescription().getVersion());
		String path = this.getFilePath();
		
		try {
			URL url = new URL(versionLink);
			URLConnection con = url.openConnection();
			InputStreamReader isr = new InputStreamReader(con.getInputStream());
			BufferedReader reader = new BufferedReader(isr);
			int newVersion = this.getVersionFromString(reader.readLine());
			reader.close();
			
			if(newVersion > oldVersion) {
				url = new URL(dlLink);
				con = url.openConnection();
				InputStream in = con.getInputStream();
				FileOutputStream out = new FileOutputStream(path);
				byte[] buffer = new byte[1024];
				int size = 0;
				while((size = in.read(buffer)) != -1) {
					out.write(buffer, 0, size);
				}
				
				out.close();
				in.close();
				startServerRestartCountdown();
			}
			else
			{
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "No Updates Found!");
			}
		} catch(IOException e) {
			plugin.getLogger().log(Level.SEVERE, "Failed to auto-update", e);
		}
	}
	
	private String getFilePath() {
		if(plugin instanceof JavaPlugin) {
			try {
				Method method = JavaPlugin.class.getDeclaredMethod("getFile");
				boolean wasAccessible = method.isAccessible();
				method.setAccessible(true);
				File file = (File) method.invoke(plugin);
				method.setAccessible(wasAccessible);
				
				return file.getPath();
			} catch(Exception e) {
				return "plugins" + File.separator + plugin.getName();
			}
		} else {
			return "plugins" + File.separator + plugin.getName();
		}
	}
	
	private int getVersionFromString(String from) {
		String result = "";
		Pattern pattern = Pattern.compile("\\d+");
		Matcher matcher = pattern.matcher(from);
		
		while(matcher.find()) {
			result += matcher.group();
		}
		
		return result.isEmpty() ? 0 : Integer.parseInt(result);
	}
	
	private void startServerRestartCountdown()
	{
		Bukkit.broadcastMessage("§f<§c*Console§f> A MineZ Update has just been Pushed Out!");
		restartID = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
			public void run()
			{
				if(restartTime != -1)
				{
					if(restartTime != 0)
					{
						Bukkit.broadcastMessage("§f<§c*Console§f> SERVER RESTARTING IN " + restartTime + " SECONDS!");
						restartTime -= 5;
					}
					else
					{
						Bukkit.getScheduler().cancelTask(restartID);
						Bukkit.shutdown();
					}
				}
			}
		}, 0L, 100L);
	}
}