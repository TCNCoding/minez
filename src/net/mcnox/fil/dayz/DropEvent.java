package net.mcnox.fil.dayz;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class DropEvent implements Listener
{
	public MineZ plugin;
	
	public DropEvent(MineZ plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerItemDrop(PlayerDropItemEvent e)
	{
		Player p = e.getPlayer();
		
		e.getItemDrop().remove();
		
		p.playSound(p.getLocation(), Sound.ENDERMAN_DEATH, 1.0F, 0.5F);
	}
}
